let links = document.getElementsByTagName('a');

function displayImg(link) {
    let img = new Image();
    let overlay = document.getElementById('overlay');

    img.addEventListener('load', function(){
        overlay.innerHTML = '';
        overlay.appendChild(img);
    });

    img.src = link.href;
    overlay.style.display = 'block';
    overlay.innerHTML = '<span>Chargement...</span>';
}

document.getElementById('overlay').addEventListener('click', function(e){
    e.currentTarget.style.display = 'none';
});

for (let i = 0; i < links.length; i++) {
    links[i].addEventListener('click', function(e) {
        e.preventDefault();

        displayImg(e.currentTarget);
    }, false);
}